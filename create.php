<!DOCTYPE HTML>
<html> 
<head> 
    <title>Create Records</title>
      
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
          
</head>
<body>
  
    <!-- container -->
    <div class="container">
   
        <div class="page-header">
            <h1>Generate Merchandise</h1>
        </div>



<!-- PHP insert code will be here -->
<?php
if($_POST){
 
    // include database connection
    include 'config/database.php';
 
    try{
     
        // insert query
        $query = "INSERT INTO portfolio SET proname=:proname, comname=:comname, modalno=:modalno, manu=:manu, bestberfore=:bestberfore, standerd=:standerd, features=:features, description=:description, price=:price, image=:image, created=:created";
 
        // prepare query for execution
        $stmt = $con->prepare($query);
 
        // posted values
        $proname=htmlspecialchars(strip_tags($_POST['proname']));
        $comname=htmlspecialchars(strip_tags($_POST['comname']));
        $modalno=htmlspecialchars(strip_tags($_POST['modalno']));
        $manu=htmlspecialchars(strip_tags($_POST['manu']));
        $bestberfore=htmlspecialchars(strip_tags($_POST['bestberfore']));
        $standerd=htmlspecialchars(strip_tags($_POST['standerd']));
        $features=htmlspecialchars(implode(",",($_POST['features'])));
        $description=htmlspecialchars(strip_tags($_POST['description']));
        $price=htmlspecialchars(strip_tags($_POST['price']));
        // image field
        $image=!empty($_FILES["image"]["name"])
            ? sha1_file($_FILES["image"]["tmp_name"]) . "_" . basename($_FILES["image"]["name"]):"";
            $image=htmlspecialchars(strip_tags($image));
 
        // bind the parameters
        $stmt->bindParam(':proname', $proname);
        $stmt->bindParam(':comname', $comname);
        $stmt->bindParam(':modalno', $modalno);
        $stmt->bindParam(':manu', $manu);
        $stmt->bindParam(':bestberfore', $bestberfore);
        $stmt->bindParam(':standerd', $standerd); 
        $stmt->bindParam(':features', $features);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':image', $image);
        $stmt->bindParam(':price', $price);
         
        // specify when this record was inserted to the database
        $created=date('Y-m-d H:i:s');
        $stmt->bindParam(':created', $created);
         
        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        if ($image) {
            $target_directory = "uploads/";
            $target_file = $target_directory . $image;
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            // error message is empty
            $file_upload_error_messages="";
            //make sure that file is real image or not
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check!=false) {
                //submitted file is an image
            }else{
                $file_upload_error_messages.="<div>Submitted file is no an image.</div>";
            }
            // sure the allowence for the files
            $allowed_file_types=array("jpg", "jpeg", "png", "gif");
            if (!in_array($file_type, $allowed_file_types)) {
                $file_upload_error_messages.="<div>Only JPG, JPEG, PNG, GIF files are allowed.</div>";
            }
            // make sure file does not exist 
            if (file_exists($target_file)) {
                $file_upload_error_messages.="<div>Image already exist.Change the file name and try again.</div>";
            }
            //size of the image not exclude
            if ($_FILES['image']['size'] > (64000000)) {
                $file_upload_error_messages.="<div>Image must b less than 8MB in size.</div>";
            }
            //sure the existence of upload folder
            //if not, create it
            if (!is_dir($target_directory)) {
                mkdir($target_directory, 0777, true);
            }
            // if error message still empty
            if (empty($file_upload_error_messages)) {
                //means there are no errors
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    //it means photo was uploaded
                }else{
                    echo "<div class='alert alert-danger'>";
                        echo "<div>Unable to upload photo.</div>";
                            echo "<div>Update the record to upload the photo.</div>";
                    echo "</div>";
                }
            }
            // error message is not empty
            else{
                // if errror then show to user
                echo "<div class='alert alert-danger'>";
                    echo "<div>{$file_upload_error_messages}</div>";
                    echo "<div> Update the record to upload the photo.</div>";
                echo "</div>";
            }
        }

        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }
         
    }
     
    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>
<!-- html form here where the product information will be entered -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data" >
    <table class='table table-hover table-responsive table-bordered'>
        <tr>
            <td>Product Name</td>
            <td><input type="text" name="proname" size="50" class='form-control' /></td>
        </tr>
        <tr>
            <td>Company Name</td>
            <td><input type="text" name="comname" size="50" class='form-control' /></td>
        </tr>
         <tr>
            <td>Modal No</td>
            <td><input type="text" name="modalno" size="50" class='form-control' /></td>
        </tr>
        <tr>
            <td>Manufactured date</td>
            <td><input type="date" name="manu" size="40" class='form-control' /></td>
        </tr>
        <tr>
            <td>Best before</td>
            <td><input type="date" name="bestberfore" size="40" class='form-control' />
            </td>
        </tr>
        <tr>
            <td>Standerd</td>
            <td><input type="radio" name="standerd" value="Genuine">Genuine
                <input type="radio" name="standerd" value="Derivative" style="margin-left: 5em;">Derivative
            </td>
        </tr>

        <tr>
            <td>Features</td>
            <td><input type="checkbox" name="features[]" value="Functionality">Functionality
                <input type="checkbox" name="features[]" value="Quality" style="margin-left: 5em;">Quality
                <input type="checkbox" name="features[]" value="Affordability" style="margin-left: 5em;">Affordability
                <input type="checkbox" name="features[]" value="Usability" style="margin-left: 5em;">Usability
                <input type="checkbox" name="features[]" value="Maintainability" style="margin-left: 5em;">Maintainability</td>
        </tr>
        <tr>
            <td>Description</td>
            <td><textarea name="description" maxlength="200" class='form-control'></textarea> </td>
        </tr>
        <tr>
            <td>Price</td>
            <td><input type='text' name='price' class='form-control' /></td>
        </tr>
         <tr> 
            <td>Photo</td>
            <td><input type='file' name='image' /></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type='submit' value='Save' class='btn-primary' />
                <a href='index.php' class='btn btn-warning'>Back to read products</a>
            </td>
        </tr>
    </table>
</form>
</div> <!-- end .container -->
      
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</body>
</html>