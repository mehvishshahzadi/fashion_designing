<!DOCTYPE html>
<html>
<head>
	<title>Update a record</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
	<div class="container">
		<div class="page header">
			<h1>Update Record</h1>
		</div>
		<!-- php code-->
		<?php 
		//use to get the pssed records through id using isset
		$id=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');
		// connecting database
		include 'config/database.php';

		// to read the curent records
		try{
			// start query
			$query= "SELECT id, proname, comname, modalno, description, standerd, features, price, image FROM portfolio where id = ? LIMIT 0,1";
			$stmt = $con->prepare($query);
			//first ?mark
			$stmt->bindParam(1, $id);
			// for execution
			$stmt->execute();

			//To stored the data rfom row
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			//taking the values to fill the form
			$proname = $row['proname'];
			$comname = $row['comname'];
			$modalno = $row['modalno'];
			$standerd = $row['standerd'];
			$features = $row['features']; 
			$description = $row['description'];
			$price = $row['price'];
			$image = $row['image'];
		}
		// for error
		catch(PDOException $exception){
			die('ERROR: ' . $exception->getMessage());
		}
		?>

		<!-- html code-->
		<!-- php post to upadte the record-->
		<?php
		// checinkg that form submitted or not
		if($_POST){
			try{
		// write update query
        // in this case, it seemed like we have so many fields to pass and 
        // it is better to label them and not use question marks
		$query = "UPDATE portfolio
					SET proname=:proname, comname=:comname, modalno=:modalno, standerd=:standerd, features=:features, description=:description, price=:price, image=:image WHERE id=:id";
					//execution query
					$stmt = $con->prepare($query);
					// posted values
        $proname=htmlspecialchars(strip_tags($_POST['proname']));
        $comname=htmlspecialchars(strip_tags($_POST['comname']));
        $modalno=htmlspecialchars(strip_tags($_POST['modalno']));
        $standerd=htmlspecialchars(strip_tags($_POST['standerd']));
        $features=htmlspecialchars(implode(",", $_POST['features']));
        $description=htmlspecialchars(strip_tags($_POST['description']));
        $price=htmlspecialchars(strip_tags($_POST['price']));
        $image=!empty($_FILES["image"]["name"])
            ? sha1_file($_FILES["image"]["tmp_name"]) . "_" . basename($_FILES["image"]["name"]):"";
            $image=htmlspecialchars(strip_tags($image));
 
        // bind the parameters
        $stmt->bindParam(':proname', $proname);
        $stmt->bindParam(':comname', $comname);
        $stmt->bindParam(':modalno', $modalno);
        $stmt->bindParam(':standerd', $standerd);
        $stmt->bindParam(':features', $features);
        $stmt->bindParam(':description', $description);
        $stmt->bindParam(':price', $price);
        $stmt->bindParam(':image', $image);
        $stmt->bindParam(':id', $id);
        //query execution
        if($stmt->execute()){
        	echo "<div class= 'alert alert-success'>Record was updated.</div>";
        }

        if ($image) {
            $target_directory = "uploads/";
            $target_file = $target_directory . $image;
            $file_type = pathinfo($target_file, PATHINFO_EXTENSION);
            // error message is empty
            $file_upload_error_messages="";
            //make sure that file is real image or not
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if ($check!=false) {
                //submitted file is an image
            }else{
                $file_upload_error_messages.="<div>Submitted file is no an image.</div>";
            }
            // sure the allowence for the files
            $allowed_file_types=array("jpg", "jpeg", "png", "gif");
            if (!in_array($file_type, $allowed_file_types)) {
                $file_upload_error_messages.="<div>Only JPG, JPEG, PNG, GIF files are allowed.</div>";
            }
            // make sure file does not exist 
            if (file_exists($target_file)) {
                $file_upload_error_messages.="<div>Image already exist.Change the file name and try again.</div>";
            }
            //size of the image not exclude
            if ($_FILES['image']['size'] > (64000000)) {
                $file_upload_error_messages.="<div>Image must b less than 8MB in size.</div>";
            }
            //sure the existence of upload folder
            //if not, create it
            if (!is_dir($target_directory)) {
                mkdir($target_directory, 0777, true);
            }
            // if error message still empty
            if (empty($file_upload_error_messages)) {
                //means there are no errors
                if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {
                    //it means photo was uploaded
                }else{
                    echo "<div class='alert alert-danger'>";
                        echo "<div>Unable to upload photo.</div>";
                            echo "<div>Update the record to upload the photo.</div>";
                    echo "</div>";
                }
            }
            // error message is not empty
            else{
                // if errror then show to user
                echo "<div class='alert alert-danger'>";
                    echo "<div>{$file_upload_error_messages}</div>";
                    echo "<div> Update the record to upload the photo.</div>";
                echo "</div>";
            }
        }


        else{
        	echo "<div class='alert alert-danger'>Unable to update record. Please try again.</div>";
        }
	}
	// show error
	catch(PDOException $exception){
		die('ERROR:' . $exception->getMessage());
	}
}

?>
		<!--  have our html form here to update the record info -->
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"] . "?id={$id}");?>" method="post" enctype="multipart/form-data" >
			<table class="table table-hover table-responsive table-bordered">
			<tr>
				<td>Product Name</td>
				<td><input type='text' name='proname' value="<?php echo htmlspecialchars($proname, ENT_QUOTES);  ?>" class='form-control' /></td>
			</tr>
			<tr>
				<td>Company Name</td>
				<td><input type='text' name='comname' value="<?php echo htmlspecialchars($comname, ENT_QUOTES);  ?>" class='form-control' /></td>
			</tr>
			<tr>
				<td>Modal No</td>
				<td><input type='text' name='modalno' value="<?php echo htmlspecialchars($modalno, ENT_QUOTES);  ?>" class='form-control' /></td>
			</tr>
 			<tr>
           		<td>Standerd</td> 
            	<td><input type="radio" name="standerd" value="Genuine" 
            	<?php if ($row["standerd"]=='Genuine') {
            		echo "checked";} ?> />Genuine

                <input type="radio" name="standerd" value="Derivative" 
				<?php if ($row["standerd"]=='Derivative') {
            		echo "checked";} ?> style="margin-left: 5em;" />Derivative
            	</td>
         	</tr> 
         	<tr>
            	<td>Features</td>
             	<?php 
            	$b = explode(",", $features);
             	?>
            	<td><input type="checkbox" name="features[]" value="Functionality" 
                	<?php if (in_array("Functionality",$b)) {
                    	echo "checked"; } ?> >Functionality
                	<input type="checkbox" name="features[]" value="Quality" <?php
                		if (in_array("Quality",$b)) {
                    		echo "checked"; } ?> style="margin-left: 5em;">Quality

                	<input type="checkbox" name="features[]" value="Affordability"<?php
                		if (in_array("Affordability",$b)) {
                    		echo "checked";}?> style="margin-left: 5em;">Affordability

                	<input type="checkbox" name="features[]" value="Usability" <?php
                		if (in_array("Usability",$b)) {
                    		echo "checked";} ?> style="margin-left: 5em;">Usability

                	<input type="checkbox" name="features[]" value="Maintainability" <?php
                		if (in_array("Maintainability",$b)) {
                    		echo "checked";} ?> style="margin-left: 5em;">Maintainability</td>
       		</tr>
			<tr>
				<td>Description</td>
				<td><textarea name="description" class="form-control"><?php echo htmlspecialchars($description, ENT_QUOTES); ?>
				</textarea></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><input type='text' name='price' value="<?php echo htmlspecialchars($price, ENT_QUOTES);  ?>" class='form-control' /></td>
			</tr>
			<tr>
        		<td>Image</td>
        		<td>
        			<input type='file' name='image' value="<?php echo htmlspecialchars($image, ENT_QUOTES); ?>" />
        			<?php echo $image ? "<img src='uploads/{$image}' style='width:300px;' />" : "NO image found."; ?>
        		</td>
        	</tr>
			<tr>
				<td></td>
				<td>
					<input type='submit' value='save changes' class='btn btn-primary' />
					<a href="index.php" class="btn btn-danger">Back to Read records</a>
				</td>
			</tr>
            <tr>
                <td></td>
                <td>
                    <input type='submit' value='save changes' class='btn btn-primary' />
                    <a href="index.php" class="btn btn-danger">Back to Read records</a>
                </td>
            </tr>
		   </table>	
		</form>
	</div>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>