 <!DOCTYPE html>
<html>
<head>
	<title> check the Records</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
</head>
<body>
	<div class="container">
		<div class="page-headwer">
			<h1>Read Record</h1>
		</div>
		<!-- php read record-->
		<?php 
		//use to get the pssed records through id using isset
		$id=isset($_GET['id']) ? $_GET['id'] : die('ERROR: Record ID not found.');
		// connecting database
		include 'config/database.php';

		// to read the curent records 
		
		try{
			// start query standerd=:standerd, features=:features,
			$query= "SELECT id, proname, comname, modalno, standerd, features, description, price, image FROM portfolio where id = ? LIMIT 0,1";
			$stmt = $con->prepare($query);
			//first ?mark
			$stmt->bindParam(1, $id);
			// for execution
			$stmt->execute();

			//To stored the data rfom row
			$row = $stmt->fetch(PDO::FETCH_ASSOC);
			//taking the values to fill the form
			$proname = $row['proname'];
			$comname = $row['comname'];
			$modalno = $row['modalno'];
			$standerd = $row['standerd'];
			$features = $row['features'];
			$description = $row['description'];
			$price = $row['price'];
			$image= htmlspecialchars($row['image'], ENT_QUOTES);
		}
		// for error
		catch(PDOException $exception){
			die('ERROR: ' . $exception->getMessage());
		}
		?>

		<!-- html read record-->
		<!-- html table to show the records-->
		<table class="table table table-hover table-responsive table-bordered">
			<tr>
				<td>Product Name</td>
				<td><?php echo htmlspecialchars($proname, ENT_QUOTES); ?></td>
			</tr>
			<tr>
				<td>Company Name</td>
				<td><?php echo htmlspecialchars($comname, ENT_QUOTES); ?></td>
			</tr>
			<tr>
				<td>Modal No</td>
				<td><?php echo htmlspecialchars($modalno, ENT_QUOTES); ?></td>
			</tr>
			<tr>
				<td>Standerd</td>
				<td><?php echo htmlspecialchars($standerd, ENT_QUOTES); ?></td>
			</tr>
			<tr>
				<td>features</td>
				<td><?php echo htmlspecialchars($features, ENT_QUOTES); ?></td>
			</tr>
			<tr>
				<td>Description</td>
				<td><?php echo htmlspecialchars($description, ENT_QUOTES); ?></td>
			</tr>
			<tr>
				<td>Price</td>
				<td><?php echo htmlspecialchars($price, ENT_QUOTES); ?></td>
			</tr>
			<tr>
        		<td>Image</td>
        		<td>
       			<?php echo $image ? "<img src='uploads/{$image}' style='width:300px;' />" : "NO image found."; ?>
        		</td>
        	</tr>
			<tr>
				<td></td>
				<td>
					<a href="index.php" class="btn btn-danger">Back to Read records</a>
				</td>
			</tr>
			
		</table>

	</div>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>