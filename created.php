<!DOCTYPE HTML>
<html>
<head>
    <title>PDO - Create a Record - PHP CRUD Tutorial</title>
      
    <!-- Latest compiled and minified Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
          
</head>
<body>
  
    <!-- container -->
    <div class="container">
   
        <div class="page-header">
            <h1>Create Product</h1>
        </div>



<!-- PHP insert code will be here -->
<?php
if($_POST){
 
    // include database connection
    include 'config/database.php';
 
    try{
     
        // insert query
        $query = "INSERT INTO portfolio SET fname=:fname, lname=:lname, email=:email, gender=:gender, age=:age, about=:about, password=:password, skilled=:skilled, price=:price, image=:image created=:created";
 
        // prepare query for execution
        $stmt = $con->prepare($query);
 
        // posted values
        $fname=htmlspecialchars(strip_tags($_POST['fname']));
        $lname=htmlspecialchars(strip_tags($_POST['lname']));
        $email=htmlspecialchars(strip_tags($_POST['email']));
        $gender=htmlspecialchars(strip_tags($_POST['gender']));
        $age=htmlspecialchars(strip_tags($_POST['age']));
        $about=htmlspecialchars(strip_tags($_POST['about']));
        $password=htmlspecialchars(strip_tags($_POST['password']));
        $skilled=htmlspecialchars(strip_tags($_POST['skilled']));
        $price=htmlspecialchars(strip_tags($_POST['price']));
 
        // bind the parameters
        $stmt->bindParam(':fname', $fname);
        $stmt->bindParam(':lname', $lname);
        $stmt->bindParam(':email', $email);
        $stmt->bindParam(':gender', $gender);
        $stmt->bindParam(':age', $age);
        $stmt->bindParam(':about', $about);
        $stmt->bindParam(':password', $password);
        $stmt->bindParam(':skilled', $skilled);
        $stmt->bindParam(':price', $price);
         
        // specify when this record was inserted to the database
        $created=date('Y-m-d H:i:s');
        $stmt->bindParam(':created', $created);
         
        // Execute the query
        if($stmt->execute()){
            echo "<div class='alert alert-success'>Record was saved.</div>";
        }else{
            echo "<div class='alert alert-danger'>Unable to save record.</div>";
        }
         
    }
     
    // show error
    catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
    }
}
?>
<!-- html form here where the product information will be entered -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" enctype="multipart/form-data">
    <table class='table table-hover table-responsive table-bordered'>
        <tr>
            <td>First Name</td>
            <td><input type="text" name="fname" size="50" class='form-control' /></td>
        </tr>
        <tr>
            <td>Last Name</td>
            <td><input type="text" name="lname" size="20" class='form-control' /></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><input type="text" name="email" size="40" class='form-control' /></td>
        </tr>
        <tr>
            <td>Gender</td>
            <td><input type="radio" name="gender" value="M" />Male<br>
            	<input type="radio" name="gender" value="F" />Female<br>
            </td>
        </tr>
        <tr>
            <td>Age</td>
            <td><select name="age" class='form-control'>
            		<option value="15-24">Under 25</option>
            		<option value="24-34">Under 35</option>
            		<option value="35-44">Under 45</option>
            		<option value="45-59">Under 60</option>
            </select>
            </td>
        </tr>
        <tr>
            <td>About Your-self</td>
            <td><textarea name="about" maxlength="200" class='form-control'></textarea> </td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password" maxlength="20" class='form-control' /></td>
        </tr>
 		<tr>
 			<td>Skilled-in</td>
            <td><input type="checkbox" name="skilled" value="Bridal Collection">Bridal Collection
            	<input type="checkbox" name="skilled" value="Groom Collection">Groom Collection
            	<input type="checkbox" name="skilled" value="Wedding Collection">Wedding Collection
             	<input type="checkbox" name="skilled" value="Party Wear"> Party Wear
              	<input type="checkbox" name="skilled" value="Casual Wear"> Casual Wear</td>
        </tr>

        <tr>
            <td>Price</td>
            <td><input type='text' name='price' class='form-control' /></td>
        </tr>
        <tr>
            <td>Photo</td>
            <td><input type='file' name='image' /></td>
        </tr>
        <tr>
            <td></td>
            <td>
                <input type='submit' value='Save' class='btn btn-primary' />
                <a href='index.php' class='btn btn-danger'>Back to read products</a>
            </td>
        </tr>
    </table>
</form>
</div> <!-- end .container -->
      
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
   
<!-- Latest compiled and minified Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
</body>
</html>